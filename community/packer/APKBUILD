# Maintainer: Galen Abell <galen@galenabell.com>
# Contributor: Galen Abell <galen@galenabell.com>
pkgname=packer
pkgver=1.7.9
pkgrel=1
pkgdesc="tool for creating machine images for multiple platforms"
url="https://www.packer.io/"
license="MPL-2.0"
arch="all"
makedepends="go"
options="net chmod-clean"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/packer/archive/v$pkgver.tar.gz"

export GOPATH="$srcdir"

build() {
	export GOFLAGS="$GOFLAGS -trimpath"
	export GOLDFLAGS="-s -w -X main.GitCommit=v$pkgver"
	go build -v -o bin/$pkgname
}

check() {
	go list . | xargs -t -n4 go test -timeout=2m -parallel=4
	bin/$pkgname -v
}

package() {
	install -Dm755 bin/"$pkgname" -t "$pkgdir"/usr/bin/
}

sha512sums="
530725b12c1aad52b63569ef6a7d8664aef47673c5ece0abd94b582014732074d9dc349bc2e9d460125cc0bd820d81fde9375f59c1d6178ca3a1cea4aca814ad  packer-1.7.9.tar.gz
"
